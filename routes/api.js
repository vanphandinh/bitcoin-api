const express = require('express');
const router = express.Router();
const BitcoinClient = require('../shared/helpers/bitcoin-client');
const bcClient = new BitcoinClient();

// GetNewAddress
router.get('/getnewaddress', (req, res) => {
    bcClient.getNewAddress()
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.send(err); 
            console.log(err);            
        });
});

// GetBalance
router.get('/getbalance', (req, res) => {
    bcClient.getBalance()
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.send(err); 
            console.log(err);            
        });
});

// Send BTC
router.get('/sendbtc', (req, res) => {
    var address = req.query.address;
    var amount = req.query.amount;
    bcClient.sendBTC(address, amount)
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.send(err); 
            console.log(err);            
        });
});

// Get list transactions
router.get('/listtransactions', (req, res) => {
    bcClient.getListTransactions()
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.send(err); 
            console.log(err);            
        });
});

// Get confirmations
router.get('/getconfirmations', (req, res) => {
    var txid = req.query.txid;
    bcClient.getConfirmations(txid)
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.send(err); 
            console.log(err);            
        });
});

module.exports = router;
