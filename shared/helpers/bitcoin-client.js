const request = require('request');

const dotenv = require('dotenv');
dotenv.config();

const USER = process.env.RPC_USER;
const PASS = process.env.RPC_PASSWORD;
const WALLET_PASSWORD = process.env.WALLET_PASSWORD;

const headers = {
    "content-type": "application/json"
};

const sendRPCRequest = (method, params) => {
    var dataString = `{"jsonrpc":"1.0","id":"bitcoin-api","method":"${method}","params": ${JSON.stringify(params)}}`;
    console.log(dataString);
    var options = {
        url: `http://${USER}:${PASS}@btc_test.websy-test.com:8332/`,
        method: "POST",
        headers: headers,
        body: dataString
    };

    return new Promise((resolve, reject) => {
        request(options, (error, response, body) => {
            if (error) reject(error);
            else {
                const data = JSON.parse(body);
                if (data.error) {
                    reject(data.error); 
                } else {
                    resolve(data.result == null ? "" : data.result);
                }
            }
        });
    });
};

class BitcoinClient {

    getNewAddress() {
        return sendRPCRequest("getnewaddress", ["bitcoin-api"]);
    }

    getBalance() {
        return sendRPCRequest("getbalance", ["*", 3]);
    }

    getListTransactions() {
        return sendRPCRequest("listtransactions", ["bitcoin-api", 999]);
    }

    sendBTC(address, amount) {
        return new Promise((resolve, reject) => {
            sendRPCRequest("walletpassphrase", [WALLET_PASSWORD, 5])
                .then(() => {
                    sendRPCRequest("sendtoaddress", [address, amount])
                        .then((data) => {
                            resolve(data);
                        })
                        .catch((err) => {
                            reject(err);  
                        });
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    getConfirmations(txid) {
        return new Promise((resolve, reject) => {
            sendRPCRequest("gettransaction", [txid, true])
                .then((data) => {
                    resolve(data.confirmations);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }
}

module.exports = BitcoinClient;
